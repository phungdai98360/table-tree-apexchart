import axios from "axios";
export default function callApiGoogle(method, url, params) {
  return axios({
    method: method,
    url: url,
    data: params,
    headers: {
      "x-rapidapi-host": "montanaflynn-fifa-world-cup.p.rapidapi.com",
      "x-rapidapi-key": "b634183833msh0180740b5b5ea9dp181503jsn59d35dea6c85",
      accepts: "json",
      useQueryString: true,
    },
  }).catch((err) => console.log(err));
}
