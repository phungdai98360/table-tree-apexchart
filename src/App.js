import React from "react";
import ListTurnPlane from "./layout/table-tree/listTurnPlane";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import Barchart from "./layout/apexcharts/barchart";
import HeaderLayout from "./components/header/index";
import UploadImage from "./layout/uploadFile/uploadImage";
import ChartRealTime from "./layout/apexcharts/chartRealTime";
import { Layout } from "antd";
const App = () => {
  return (
    <Layout className="layout">
      <Router>
        <HeaderLayout />
        <Switch>
          <Route exact path="/">
            <ListTurnPlane />
          </Route>
          <Route path="/chart">
            <Barchart />
          </Route>
          <Route path="/upload">
            <UploadImage />
          </Route>
          <Route path="/chartrealtime">
            <ChartRealTime />
          </Route>
        </Switch>
      </Router>
    </Layout>
  );
};

export default App;
