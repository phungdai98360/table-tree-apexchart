import React, { useState } from "react";
import PropTypes from "prop-types";
import { Layout, Menu } from "antd";
import { NavLink } from "react-router-dom";
const { Header, Content, Footer } = Layout;
const HeaderLayout = (props) => {
  return (
    <Header>
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
        <Menu.Item key="1">
          <NavLink to="/">Home</NavLink>
        </Menu.Item>
        <Menu.Item key="2">
          <NavLink to="/chart">Chart</NavLink>
        </Menu.Item>
        <Menu.Item key="3">
          <NavLink to="/upload">Upload</NavLink>
        </Menu.Item>
        <Menu.Item key="4">
          <NavLink to="/chartrealtime">Total Covid 2019</NavLink>
        </Menu.Item>
      </Menu>
    </Header>
  );
};

export default HeaderLayout;
