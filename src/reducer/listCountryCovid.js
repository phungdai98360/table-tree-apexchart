import * as actionType from "./../actions/actionType";
const initialState = {
  countries: [],
  data: [],
};
const listCountriesCovid = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FET_LIST_COUNTRIES_SUCCESS:
      const arrCountries = [];
      const arrData = [];

      for (let i = 0; i < action.data.length; i++) {
        if (parseInt(action.data[i].TotalConfirmed) > 50000) {
          arrCountries.push(action.data[i].Country);
          arrData.push(action.data[i].TotalConfirmed);
        }
      }
      state.countries = [...arrCountries];
      state.data = [...arrData];
      return { ...state };
    case actionType.FET_LIST_COUNTRIES_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default listCountriesCovid;
