import * as actionType from "./../actions/actionType";
const initialState = {
  loading: false,
  dataTotalCovid: [],
};
const dataChartRealTime = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FET_LIST_DATA_REAL_TIME_SUCCESS:
      const temp = [
        parseInt(action.data.TotalConfirmed),
        parseInt(action.data.TotalDeaths),
        parseInt(action.data.TotalRecovered),
      ];
      state.dataTotalCovid = [...temp];
      state.loading = false;
      console.log(state.dataTotalCovid);
      return { ...state };
    case actionType.FET_LIST_DATA_REAL_TIME_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default dataChartRealTime;
