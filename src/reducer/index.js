import { combineReducers } from "redux";
import listTurnPlane from "./turnPlane";
import listCountriesCovid from "./listCountryCovid";
import listTeamAndRound from "./teamSoccer";
import dashboardGold from "./goldSoccer";
import dataChartRealTime from "./dataChartRealTime";
const reducer = combineReducers({
  listTurnPlane,
  listCountriesCovid,
  listTeamAndRound,
  dashboardGold,
  dataChartRealTime,
});
export default reducer;
