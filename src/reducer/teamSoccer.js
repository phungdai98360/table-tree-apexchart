import * as actionType from "./../actions/actionType";
const initialState = {
  loading: false,
  listRound: [],
};
const listTeamAndRound = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FET_LIST_TEAM_SOCCER:
      state.loading = true;
      return { ...state };
    case actionType.FET_LIST_TEAM_SOCCER_SUCCESS:
      const arrTemp = [];
      console.log("data reducer:", action.dataRoundSoccer);
      console.log("dataTeam reducer:", action.dataTeamSoccer);
      for (let i = 0; i < action.dataRoundSoccer.length; i++) {
        let index = action.dataTeamSoccer.findIndex(
          (team) => team.id === action.dataRoundSoccer[i].team1_id,
        );
        let index2 = action.dataTeamSoccer.findIndex(
          (team) => team.id === action.dataRoundSoccer[i].team2_id,
        );
        arrTemp.push({
          ...action.dataRoundSoccer[i],
          team1: { ...action.dataTeamSoccer[index] },
          team2: { ...action.dataTeamSoccer[index2] },
        });
      }
      state.listRound = [...arrTemp];
      state.loading = false;
      return { ...state };
    case actionType.FET_LIST_TEAM_SOCCER_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default listTeamAndRound;
