import * as actionType from "./../actions/actionType";
const initialState = {
  listPerson: [],
  listGold: [],
};
const dashboardGold = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FET_LIST_GOLD_SUCCESS:
      const dataPerson = action.dataPerson;
      const dataGold = action.dataGold;
      const arrtemp = [];
      const person = [];
      const gold = [];
      // console.log("data reducer : ", dataPerson);
      // console.log("data reducer : ", dataGold);
      for (let i = 0; i < dataPerson.length; i++) {
        let index = dataGold.findIndex(
          (gold) => gold.person_id === dataPerson[i].id,
        );
        arrtemp.push({
          ...dataPerson[i],
          score:
            parseInt(dataGold[index].score1) + parseInt(dataGold[index].score2),
        });
      }
      //console.log(arrtemp);
      for (let j = 0; j < arrtemp.length; j++) {
        person.push(arrtemp[j].name);
        gold.push(arrtemp[j].score);
      }
      state.listPerson = [...person];
      state.listGold = [...gold];
      return { ...state };
    case actionType.FET_LIST_GOLD_FAIL:
      return { ...state };
    default:
      return { ...state };
  }
};
export default dashboardGold;
