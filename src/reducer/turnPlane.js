import * as actionType from "./../actions/actionType";
const initialState = {
  list: [],
  statusEdit: false,
};
const listTurnPlane = (state = initialState, action) => {
  switch (action.type) {
    case actionType.FET_TURN_PLANE_SUCCESS:
      state.list = action.data;
      return { ...state };
    case actionType.FET_TURN_FAIL:
      return { ...state };

    default:
      return { ...state };
  }
};
export default listTurnPlane;
