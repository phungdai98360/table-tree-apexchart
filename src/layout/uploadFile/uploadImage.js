import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  Layout,
  Upload,
  message,
  Spin,
  Button,
  Input,
  Space,
  Table,
  Row,
  Col,
} from "antd";
import Highlighter from "react-highlight-words";
import {
  SearchOutlined,
  PlusCircleOutlined,
  LoadingOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import callApiGoogle from "./../../until/callApiGoogle";
import { connect } from "react-redux";
import * as actionType from "./../../actions/actionType";
const { Content } = Layout;
function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result));
  reader.readAsDataURL(img);
}
function beforeUpload(file) {
  const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
  if (!isJpgOrPng) {
    message.error("You can only upload JPG/PNG file!");
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error("Image must smaller than 2MB!");
  }
  return isJpgOrPng && isLt2M;
}
const UploadImage = (props) => {
  const [loading, setLoading] = useState(false);
  const [imageUrlState, setImageUrlState] = useState(null);
  const [searchText, setsearchText] = useState("");
  const [searchedColumn, setsearchedColumn] = useState("");
  const handleChange = (info) => {
    if (info.file.status === "uploading") {
      setLoading(true);
      return;
    }
    if (info.file.status === "done") {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl) => {
        setImageUrlState(imageUrl);
        setLoading(false);
      });
    }
  };
  useEffect(() => {
    // callApiGoogle(
    //   "GET",
    //   "https://montanaflynn-fifa-world-cup.p.rapidapi.com/teams",
    //   null,
    // ).then((res) => {
    //   console.log(res.data);
    // });
    props.getListRoundSoccer();
  }, []);
  console.log(props.listTeamAndRoundSoccer);
  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className="ant-upload-text">Upload</div>
    </div>
  );
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        //setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setsearchText(selectedKeys[0]);
    setsearchedColumn(dataIndex);
  };
  const handleReset = (clearFilters) => {
    clearFilters();
    setsearchText("");
  };
  const columns = [
    {
      title: "Ngay",
      dataIndex: "play_at",
      key: "play_at",

      ...getColumnSearchProps("play_at"),
    },
    {
      title: "Team 1",
      key: "team1.title",
      render: (text, record) => {
        return record.team1.title;
      },
    },
    {
      title: "Team 2",
      render: (text, record) => {
        return record.team2.title;
      },
    },
  ];
  return (
    <Content style={{ padding: "0 50px" }}>
      <Upload
        name="avatar"
        listType="picture-card"
        className="avatar-uploader"
        showUploadList={false}
        action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
        beforeUpload={beforeUpload}
        onChange={handleChange}
      >
        {imageUrlState ? (
          <img src={imageUrlState} alt="avatar" style={{ width: "100%" }} />
        ) : (
          uploadButton
        )}
      </Upload>
      {props.listTeamAndRoundSoccer.loading && (
        <Spin
          tip="Loading..."
          style={{ width: "100%", height: "100%", textAlign: "center" }}
        ></Spin>
      )}
      <Row>
        <Col xs={24}>
          <Table
            columns={columns}
            dataSource={props.listTeamAndRoundSoccer.listRound}
          />
        </Col>
      </Row>
    </Content>
  );
};

UploadImage.propTypes = {};
const mapStateToProps = (state) => {
  return {
    listTeamAndRoundSoccer: state.listTeamAndRound,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getListRoundSoccer: () => {
      dispatch({
        type: actionType.FET_LIST_TEAM_SOCCER,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(UploadImage);
