import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import Chart from "react-apexcharts";
import { Row, Col } from "antd";
import { connect } from "react-redux";
import callApi from "./../../until/callApi";
import * as actionType from "./../../actions/actionType";
import { Layout } from "antd";
const { Content } = Layout;
const Barchart = (props) => {
  useEffect(() => {
    props.getListCountriesCovid();
    props.getPersonAndGold();
    // callApi("GET", "https://api.covid19api.com/summary", null).then((res) => {
    //   console.log(res.data.Countries);
    // });
  }, []);
  console.log(props.dashboardGold);
  return (
    <Content style={{ padding: "0 50px" }}>
      <h1>Biểu đồ thống kê dịch covid 19 trên thế giới</h1>
      <Row>
        <Col xs={24} lg={12}>
          <Chart
            options={{
              chart: {
                id: "apexchart-example",
              },
              xaxis: {
                categories: props.listCountriesCovid.countries,
              },
              labels: props.listCountriesCovid.countries,
            }}
            series={[
              {
                name: "",
                data: props.listCountriesCovid.data,
              },
            ]}
            type="bar"
            height={600}
          ></Chart>
        </Col>
        <Col xs={24} lg={12}>
          <Chart
            options={{
              labels: props.listCountriesCovid.countries,
            }}
            series={props.listCountriesCovid.data}
            type="donut"
            height={380}
          ></Chart>
        </Col>
      </Row>
      <Row>
        <Col xs={24} lg={12}>
          <Chart
            options={{
              labels: props.listCountriesCovid.countries,
            }}
            series={props.listCountriesCovid.data}
            type="pie"
            height={380}
          ></Chart>
        </Col>
        <Col xs={24} lg={12}>
          <Chart
            options={{
              chart: {
                id: "apexchart-example",
              },
              xaxis: {
                categories: props.dashboardGold.listPerson,
              },
              labels: props.dashboardGold.listPerson,
            }}
            series={[
              {
                name: "",
                data: props.dashboardGold.listGold,
              },
            ]}
            type="bar"
            height={600}
          ></Chart>
        </Col>
      </Row>
    </Content>
  );
};

const mapStateToProps = (state) => {
  return {
    listCountriesCovid: state.listCountriesCovid,
    dashboardGold: state.dashboardGold,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getListCountriesCovid: () => {
      dispatch({
        type: actionType.FET_LIST_COUNTRIES,
      });
    },
    getPersonAndGold: () => {
      dispatch({
        type: actionType.FET_LIST_GOLD,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Barchart);
