import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Chart from "react-apexcharts";
import { connect } from "react-redux";
import { Layout, Row, Col } from "antd";

import * as actionType from "./../../actions/actionType";
const { Content } = Layout;
const title = [
  "Tổng số người mức bệnh",
  "Tổng số người chết",
  "Tổng số người đã được chữa khỏi",
];
const ChartRealTime = (props) => {
  useEffect(() => {
    // setInterval(() => {
    //   props.fetDataRealTime();
    // }, 5000);
    props.fetDataRealTime();
  }, []);

  //console.log(props.dataChartRealTime.dataTotalCovid);
  return (
    <Content>
      <Row>
        <Col xs={24}>
          <Chart
            options={{
              chart: {
                id: "apexchart-example",
              },
              xaxis: {
                categories: title,
              },
            }}
            series={[
              {
                name: "",
                data: props.dataChartRealTime.dataTotalCovid,
              },
            ]}
            type="bar"
            height={600}
          ></Chart>
        </Col>
      </Row>
    </Content>
  );
};

ChartRealTime.propTypes = {};
const mapStateToProps = (state) => {
  return {
    dataChartRealTime: state.dataChartRealTime,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    fetDataRealTime: () => {
      dispatch({
        type: actionType.FET_LIST_DATA_REAL_TIME,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ChartRealTime);
