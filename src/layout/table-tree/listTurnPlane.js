import React, { useEffect } from "react";
// import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Table, Button, Row, Col, Layout } from "antd";
import { DownOutlined } from "@ant-design/icons";
import * as actionType from "./../../actions/actionType";
const { Content } = Layout;
const ListTurnPlane = (props) => {
  console.log(process.env.REACT_APP_URL_API);
  const expandedRowRenderVes = (ves) => {
    const columns = [
      { title: "Mã vé", dataIndex: "mave", key: "mave" },
      { title: "Số ghế", dataIndex: "soghe", key: "soghe" },
    ];

    return (
      <Table
        rowKey={(record) => record.mave}
        columns={columns}
        dataSource={ves}
      />
    );
  };
  const columnsTurnPlane = [
    { title: "Mã chuyến bay", dataIndex: "macb", key: "macb" },
    { title: "Thời gian đi", dataIndex: "tgdi", key: "tgdi" },
    { title: "Thời gian đên", dataIndex: "tgden", key: "tgden" },
    {
      title: "Hành động",
      key: "action",
      render: (text, record) => (
        <React.Fragment>
          <Button>Sửa</Button>
          <Button>Xoá</Button>
        </React.Fragment>
      ),
    },
  ];
  useEffect(() => {
    props.getListTurnPlane();
  }, []);
  console.log(props.listTurnPlane);
  return (
    <Content style={{ padding: "0 50px" }}>
      <Table
        className="components-table-demo-nested"
        columns={columnsTurnPlane}
        expandable={{
          expandedRowRender: (record) => expandedRowRenderVes(record.ves),
        }}
        rowKey={(record) => record.macb}
        dataSource={props.listTurnPlane}
      />
    </Content>
  );
};
//
// listTurnPlane.propTypes = {};
const mapStateToProps = (state) => {
  return {
    listTurnPlane: state.listTurnPlane.list,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getListTurnPlane: () => {
      dispatch({
        type: actionType.FET_TURN_PLANE,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ListTurnPlane);
