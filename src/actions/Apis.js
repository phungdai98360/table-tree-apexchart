import callApi from "./../until/callApi";
import callApiGoogle from "./../until/callApiGoogle";
import {
  GETTURNPLANE,
  GETCOUNTRIESCOVID,
  GETTEAMSOCCER,
  GETROUNDSOCCER,
  GETPERSONSOCCER,
  GETGOLD,
} from "./../config/configApi";
function* getTurnPlaneFromApi() {
  const res = yield callApi("GET", GETTURNPLANE, null);
  const turnPlane = res.data;
  return turnPlane;
}
function* getCountriesCovidFromApi() {
  const res = yield callApi("GET", GETCOUNTRIESCOVID, null);
  const dataCovid = res.data.Countries;

  return dataCovid;
}
function* getTeamSoccerFromApi() {
  const res = yield callApiGoogle("GET", GETTEAMSOCCER, null);
  const dataTeamSoccer = res.data;
  return dataTeamSoccer;
}
function* getRoundSoccerFromApi() {
  const res = yield callApiGoogle("GET", GETROUNDSOCCER, null);
  const dataRoundSoccer = res.data;
  return dataRoundSoccer;
}
function* getPersonFromApi() {
  const res = yield callApiGoogle("GET", GETPERSONSOCCER, null);
  const dataPerson = res.data;
  return dataPerson;
}
function* getGoldFromApi() {
  const res = yield callApiGoogle("GET", GETGOLD, null);
  const dataGold = res.data;
  return dataGold;
}
function* getDataCovidFromApi() {
  const res = yield callApi("GET", GETCOUNTRIESCOVID, null);
  const dataRealTime = res.data.Global;

  return dataRealTime;
}
export const Api = {
  getTurnPlaneFromApi,
  getCountriesCovidFromApi,
  getTeamSoccerFromApi,
  getRoundSoccerFromApi,
  getPersonFromApi,
  getGoldFromApi,
  getDataCovidFromApi,
};
