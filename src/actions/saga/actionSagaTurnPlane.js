import * as actionType from "./../actionType";
import { put, takeLatest, call } from "redux-saga/effects";
import { Api } from "./../Apis";
import { Button, notification, Space } from "antd";
const openNotificationWithIcon = (type, message) => {
  notification[type]({
    message: "Thông báo",
    description: message,
  });
};
function* fetTurnPlane() {
  try {
    const data = yield Api.getTurnPlaneFromApi();
    yield put({
      type: actionType.FET_TURN_PLANE_SUCCESS,
      data: data,
    });
    openNotificationWithIcon("success", "Lấy dữ liệu thành công");
  } catch (error) {
    yield put({
      type: actionType.FET_TURN_FAIL,
    });
    openNotificationWithIcon("error", "Lấy dữ liệu thất bại");
  }
}
function* fetCountriesCovid() {
  try {
    const data = yield Api.getCountriesCovidFromApi();
    yield put({
      type: actionType.FET_LIST_COUNTRIES_SUCCESS,
      data: data,
    });
    openNotificationWithIcon("success", "Lấy dữ liệu thành công");
  } catch (error) {
    yield put({
      type: actionType.FET_LIST_COUNTRIES_FAIL,
    });
    openNotificationWithIcon("error", "Lấy dữ liệu thất bại");
  }
}
function* fetTeamAndRound() {
  try {
    const dataTeamSoccer = yield Api.getTeamSoccerFromApi();
    const dataRoundSoccer = yield Api.getRoundSoccerFromApi();
    yield put({
      type: actionType.FET_LIST_TEAM_SOCCER_SUCCESS,
      dataTeamSoccer: dataTeamSoccer,
      dataRoundSoccer: dataRoundSoccer,
    });
    openNotificationWithIcon("success", "Lấy dữ liệu thành công");
  } catch (error) {
    yield put({
      type: actionType.FET_LIST_TEAM_SOCCER_FAIL,
    });
    openNotificationWithIcon("error", "Lấy dữ liệu thất bại");
  }
}
function* fetPersonAndGold() {
  try {
    const dataPerson = yield Api.getPersonFromApi();
    const dataGold = yield Api.getGoldFromApi();
    yield put({
      type: actionType.FET_LIST_GOLD_SUCCESS,
      dataPerson: dataPerson,
      dataGold: dataGold,
    });
    openNotificationWithIcon("success", "Lấy dữ liệu thành công");
  } catch (error) {
    console.log("Loox action saga");
    yield put({
      type: actionType.FET_LIST_GOLD_FAIL,
    });
    openNotificationWithIcon("error", "Lấy dữ liệu thất bại");
  }
}
function* fetDataRealTime() {
  try {
    const data = yield Api.getDataCovidFromApi();
    yield put({
      type: actionType.FET_LIST_DATA_REAL_TIME_SUCCESS,
      data: data,
    });
  } catch (error) {
    yield put({
      type: actionType.FET_LIST_DATA_REAL_TIME_FAIL,
    });
    openNotificationWithIcon("error", "Lấy dữ liệu thất bại");
  }
}
export function* watchFetTurnPlane() {
  yield takeLatest(actionType.FET_TURN_PLANE, fetTurnPlane);
}
export function* watchFetCountriesCovid() {
  yield takeLatest(actionType.FET_LIST_COUNTRIES, fetCountriesCovid);
}
export function* watchFetTeamAndRoundSoccer() {
  yield takeLatest(actionType.FET_LIST_TEAM_SOCCER, fetTeamAndRound);
}
export function* watchFetPersonAndGold() {
  yield takeLatest(actionType.FET_LIST_GOLD, fetPersonAndGold);
}
export function* watchFetDataRealTime() {
  yield takeLatest(actionType.FET_LIST_DATA_REAL_TIME, fetDataRealTime);
}
