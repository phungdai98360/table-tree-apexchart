import { all } from "redux-saga/effects";
import {
  watchFetTurnPlane,
  watchFetCountriesCovid,
  watchFetTeamAndRoundSoccer,
  watchFetPersonAndGold,
  watchFetDataRealTime,
} from "./actionSagaTurnPlane";
export default function* rootSaga() {
  yield all([
    watchFetTurnPlane(),
    watchFetCountriesCovid(),
    watchFetTeamAndRoundSoccer(),
    watchFetPersonAndGold(),
    watchFetDataRealTime(),
  ]);
}
