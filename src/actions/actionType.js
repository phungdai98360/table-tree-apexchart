export const FET_TURN_PLANE = "FET_TURN_PLANE";
export const FET_TURN_PLANE_SUCCESS = "FET_TURN_PLANE_SUCCESS";
export const FET_TURN_FAIL = "FET_TURN_FAIL";
export const FET_LIST_COUNTRIES = "FET_LIST_COUNTRIES";
export const FET_LIST_COUNTRIES_SUCCESS = "FET_LIST_COUNTRIES_SUCCESS";
export const FET_LIST_COUNTRIES_FAIL = "FET_LIST_COUNTRIES_FAIL";
export const FET_LIST_TEAM_SOCCER = "FET_LIST_TEAM_SOCCER";
export const FET_LIST_TEAM_SOCCER_SUCCESS = "FET_LIST_TEAM_SOCCER_SUCCESS";
export const FET_LIST_TEAM_SOCCER_FAIL = "FET_LIST_TEAM_SOCCER_FAIL";
export const FET_LIST_GOLD = "FET_LIST_GOLD";
export const FET_LIST_GOLD_SUCCESS = "FET_LIST_GOLD_SUCCESS";
export const FET_LIST_GOLD_FAIL = "FET_LIST_GOLD_FAIL";
export const FET_LIST_DATA_REAL_TIME = "FET_LIST_DATA_REAL_TIME";
export const FET_LIST_DATA_REAL_TIME_SUCCESS =
  "FET_LIST_DATA_REAL_TIME_SUCCESS";
export const FET_LIST_DATA_REAL_TIME_FAIL = "FET_LIST_DATA_REAL_TIME_FAIL";
