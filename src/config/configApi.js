//const API_URL = "http://datvemaybay.somee.com/api/";
export const GETTURNPLANE =
  process.env.REACT_APP_URL_API + "chuyen-bay/get-all";
export const GETCOUNTRIESCOVID = "https://api.covid19api.com/summary";
export const GETTEAMSOCCER =
  "https://montanaflynn-fifa-world-cup.p.rapidapi.com/teams";
export const GETROUNDSOCCER =
  "https://montanaflynn-fifa-world-cup.p.rapidapi.com/games";
export const GETPERSONSOCCER =
  "https://montanaflynn-fifa-world-cup.p.rapidapi.com/persons";
export const GETGOLD =
  "https://montanaflynn-fifa-world-cup.p.rapidapi.com/goals";
